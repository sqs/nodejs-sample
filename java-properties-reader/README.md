#  [![Build Status](https://secure.travis-ci.org/gsdenys/java-properties-reader.png?branch=master)](http://travis-ci.org/gsdenys/java-properties-reader)

> Another example how to read and parse java properties file


## Getting Started

Install the module with: `npm install java-properties-reader`

```js
var java-properties-reader = require('java-properties-reader');
java-properties-reader.awesome(); // "awesome"
```

Install with cli command

```sh
$ npm install -g java-properties-reader
$ java-properties-reader --help
$ java-properties-reader --version
```




## Documentation

_(Coming soon)_


## Examples

_(Coming soon)_


## Contributing

In lieu of a formal styleguide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code using [Grunt](http://gruntjs.com).


## License

Copyright (c) 2014 FassoF LTDA
Licensed under the MIT license.
