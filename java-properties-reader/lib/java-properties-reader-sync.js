/*
 * 
 * http://www.fassof.com
 *
 * Copyright (c) 2014 Denys G Santos
 * Licensed under the MIT license.
 */
 
'use strict';

var properties = require("properties");
var fs = require ("fs");

exports.read = function(fileName, parameters) {
	console.log("PROPERTIES START: " + fileName);
	
	var options = {
		path : false,
	    sections: true,
	    namespaces: true,
	};

	var data = fs.readFileSync(fileName, { encoding: "utf8" });
	var rtn = properties.parse(data, options);
	
	console.log(rtn);
	
	console.log("PROPERTIES END " + fileName);	
};