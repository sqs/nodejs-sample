/*
 * 
 * http://www.fassof.com
 *
 * Copyright (c) 2014 Denys G Santos
 * Licensed under the MIT license.
 */
 
'use strict';

var properties = require("properties");

exports.read = function(fileName, parameters) {
    console.log("PROPERTIES SYNC START: " + fileName);

	//options
	var options = {
		path : true,
	    sections: true,
	    namespaces: true,
	};
	
	//callback for response read
	var callback = function (error, obj){
		if (error) return console.error (error);

		console.log(">>> CALLBACK START");
		console.log(obj);
		console.log(">>> CALLBACK END");
	};
	
	properties.parse (fileName, options, callback);
	
	console.log("PROPERTIES SYNC END " + fileName);
};