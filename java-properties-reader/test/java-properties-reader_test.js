/*global describe,it*/
'use strict';
var assert = require('assert'),
  javaPropertiesReader = require('../lib/java-properties-reader.js');

describe('java-properties-reader node module.', function() {
  it('must be awesome', function() {
    assert( javaPropertiesReader.awesome(), 'awesome');
  });
});
