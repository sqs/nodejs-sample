'use strict';

console.log("\n\n");
console.log("#######################################");
console.log("###                                 ###");
console.log("###      JAVA PROPERTIES READ       ###");
console.log("###                                 ###");
console.log("#######################################");

var javaPropertiesReader = require('../lib/java-properties-reader.js');
var javaPropertiesReaderSync = require('../lib/java-properties-reader-sync.js');

console.log("\n\n-----------------------");
console.log("PROPERTIES SYNC EXECUTION");
javaPropertiesReaderSync.read('../example/config.properties', null);

console.log("\n\n-----------------------");
console.log("PROPERTIES EXECUTION");
javaPropertiesReader.read('../example/config.properties', null);